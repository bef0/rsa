package test;

import rsa.RSA;

public class Example {
	public static void main(String[] args) {
		RSA rsa = new RSA(5, 17, 5);
//		System.out.println("d=" + rsa.d + " e=" + rsa.e + " phi(N)=" + rsa.phiN);
//		System.out.println(rsa.encrypt(7, 23, 143));
		int encrypt = rsa.encrypt(12);
		System.out.println(encrypt);
		System.out.println(rsa.decrypt(encrypt));
		
		RSA rsa2 = new RSA(47, 23, 143, 120);
		encrypt = rsa2.encrypt(12);
		System.out.println(encrypt);
		System.out.println(rsa2.decrypt(encrypt));
	}
}
