package test;

import static org.junit.Assert.*;

import org.junit.Test;

import erweiterterEukl.*;
import rsa.RSA;

public class TestRSAProject {

	@Test
	public void testRSA() {
		try {
			new RSA(1,2,3);
		} catch(RuntimeException e) {
			assertTrue(true);
		}
		
		RSA rsa = new RSA(5, 17, 5);
		int encrypt = rsa.encrypt(12);
		assertTrue(rsa.decrypt(encrypt) == 12);
		
		
		RSA rsa2 = new RSA(47, 23, 143, 120);
		assertTrue(rsa2.getE() == 23);
		encrypt = rsa2.encrypt(12);
		assertTrue(rsa2.decrypt(encrypt) == 12);
		assertTrue(rsa.decrypt(12, 47, 143) == 12);
		
		assertTrue(rsa.decrypt(rsa2.encrypt(20, rsa.getE(), rsa.getN())) == 20);
		
	}
	
	// further tests
	
	@Test
	public void testSquareAndMultiply() {
		assertTrue(squareAndMultiply.SquareAndMultiply.squareAndMultiply(10, 5) == 100000);
	}
	
	@Test
	public void testErweiterterEukl() {
		ErweiterterEukl eEukl = new ErweiterterEukl();
		Linearkombination lk = eEukl.calc(33, 38);
		assertTrue(lk.getValues()[0] == 15 && lk.getValues()[1] == 33);
		
		assertTrue(lk.toString().equals("15 * 33 + -13 * 38 = 1"));
		
		lk = eEukl.calc(92, 69);
		assertTrue(lk.toString().equals("1 * 92 + -1 * 69 = 23"));
	}

}
