package squareAndMultiply;

public class SquareAndMultiply {
	public static int squareAndMultiply(int basis, int exponent, boolean modRechnen, int mod) {
		String exponentAsStr = Integer.toBinaryString(exponent);
		int zahl = 1;

		for (int i = 0; i < exponentAsStr.length(); i++) {
			if (exponentAsStr.charAt(i) == '0') {
				zahl *= zahl;
				if (modRechnen)
					zahl = zahl % mod;
			} else if (exponentAsStr.charAt(i) == '1') {
				zahl *= zahl;
				if (modRechnen)
					zahl = zahl % mod;
				zahl *= basis;
				if (modRechnen)
					zahl = zahl % mod;
			} else {
				throw new IllegalArgumentException("Illegal byte for exponent!"); // optional
			}
		}

		return zahl;
	}

	public static int squareAndMultiply(int basis, int exponent) {
		return squareAndMultiply(basis, exponent, false, 0);
	}

	public static int squareAndMultiply(int basis, int exponent, int mod) {
		return squareAndMultiply(basis, exponent, true, mod);
	}
}
