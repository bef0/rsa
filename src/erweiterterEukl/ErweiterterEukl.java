package erweiterterEukl;

import java.util.Stack;

public class ErweiterterEukl {

	public Linearkombination calc(int a, int b) {
		Stack<LineInErweiterterEukl> euklid = euklidAlg(a, b);
		nachRestenAufloesen(euklid);
		return linearkombination(euklid);
	}

	public Linearkombination calc(Stack<LineInErweiterterEukl> euklid) {
		nachRestenAufloesen(euklid);
		return linearkombination(euklid);
	}

	public Stack<LineInErweiterterEukl> euklidAlg(int a, int b) {
		Stack<LineInErweiterterEukl> euklid = new Stack<>();
		do {
			euklid.add(new LineInErweiterterEukl(a, b));
		} while ((b = euklid.peek().getR()) > 0 && (a = euklid.peek().getB()) != 0);

		return euklid;
	}
	
	public void nachRestenAufloesen(Stack<LineInErweiterterEukl> euklid) {
		for (int i = 0; i < euklid.size(); i++)
		{
			euklid.elementAt(i).nachRestAufloesen();
		}
	}

	public Linearkombination linearkombination(Stack<LineInErweiterterEukl> nachRest) {
		Linearkombination lk = null;
		while (!nachRest.empty()) {
			LineInErweiterterEukl nr = nachRest.pop();
			if (nr.getR() == 0)
				continue;

			if (lk == null) {
				lk = new Linearkombination(nr.getR(), 1, nr.getA(), nr.getN(), nr.getB());
			} else {
				lk.einsetzen(nr.getR(), nr.getA(), nr.getN(), nr.getB());
			}

		}

		return lk;
	}
}
