package erweiterterEukl;

public class LineInErweiterterEukl {
	// 349 = 3 * 100 + 49
	// a = n * b + r
	private int a, n, b, r;
	private boolean nachRestSolved = false;

	public LineInErweiterterEukl(int a, int b) {
		// ein Tauschen der Werte, wenn a < b ist, ist nicht erforderlich
		this.a = a;
		this.b = b;
		n = a / b;
		if (n <= 0) {
			// damit keine weiteren if-Bedingungen entstehen, wird mit 'multi' gerechnet
			// a > 0 ==> multi = 1 ==> n += multi <=> n++
			// a < 0 ==> multi = -1 ==> n += multi <=> n--
			int multi = 1;
			while ((b > 0 && a > 0 && ((n + 1) * b) < a)
					|| (b > 0 && a < 0 && ((n + 1) * b) > a && (multi = -1) == -1)) {
				n += multi;
			}
		}
		r = a - (n * b);
	}
	
	public void nachRestAufloesen() {
		if (nachRestSolved)
			return;
		
		n = getN() * (-1);
		nachRestSolved = true;
	}

	public int getA() {
		return a;
	}

	public int getN() {
		return n;
	}

	public int getB() {
		return b;
	}

	public int getR() {
		return r;
	}
}
