package erweiterterEukl;

public class Linearkombination {
	// 1 = 1 * 49 - 24 * 2
	// r = n1 * a1 - n2 * a2
	private int r, n1, a1, n2, a2;

	public Linearkombination(int r, int n1, int a1, int n2, int a2) {
		this.r = r;
		this.n1 = n1;
		this.a1 = a1;
		this.n2 = n2;
		this.a2 = a2;
	}

	public void einsetzen(int ziel, int a, int n, int b) {
		// 100 - 2 * 49
		// a - n * b

		if (a2 == ziel && b == a1) // es wird fuer a2 eingesetzt, "links kommt nach rechts", Standardfall
		{
			// nt1 und nt2 müssen hier initalisiert und deklariert werden, weil sonst auf falschen
			// Werten gearbeitet wird
			int nt1, nt2; // t = temp
			nt1 = n2;
			nt2 = n2 * n;

			// es findet gleichzeitig ein 'Tausch' statt
			n2 = nt2 + n1;
			n1 = nt1; // muss hier stehen, weil sonst n2 falschen Wert bekommt
			a1 = a;
			a2 = b;
		} else {
			throw new RuntimeException("Es kann kein Wert für 'ziel' gefunden werden.");
		}
	}

	@Override
	public String toString() {
		return n1 + " * " + a1 + " + " + n2 + " * " + a2 + " = " + r;
	}

	/**
	 * @return [n1, a1, n2, a2, r]
	 */
	public int[] getValues() {
		int[] re = { n1, a1, n2, a2, r };
		return re;
	}
}
