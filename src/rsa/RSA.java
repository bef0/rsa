package rsa;

import erweiterterEukl.*;
//import erweiterterEukl.ErweiterterEukl.Linearkombination;
import squareAndMultiply.*;

public class RSA {
	private int d, e, N, phiN;

	public RSA(int d, int e, int N, int phiN) {
		init(d, e, N, phiN);
	}

	public RSA(int p, int q, int e) {
		init(p, q, e);
	}
	
	/**
	 * @return public key (one part)
	 */
	public int getE() {
		return e;
	}
	
	/**
	 * @return public key (other part)
	 */
	public int getN() {
		return N;
	}
	
	private void init(int d, int e, int N, int phiN) {
		this.d = d;
		this.e = e;
		this.N = N;
		this.phiN = phiN;
		createPrivateKey();
	}
	
	private void init(int p, int q, int e) {
		N = p * q;
		phiN = (p - 1) * (q - 1);
		this.e = e;
		
		createPrivateKey();		
	}

	private void createPrivateKey()
	{
		d = generatePrivateKey(phiN, e);
		
		if (!keyCheck(d, e, phiN)) {
			throw new RuntimeException("Die Schlüsselwerte sind ungültig.");
		}
	}
	
	private int generatePrivateKey(int phiN, int e) {
		ErweiterterEukl eEukl = new ErweiterterEukl();
		Linearkombination lk;
		lk = eEukl.calc(e, phiN);
		int d = lk.getValues()[0];
		if (d < 0)
			d += phiN;

		return d;
	}

	private boolean keyCheck(double d, double e, double phiN) {
		return ((e * d) % phiN) == 1.0;
	}

	public int encrypt(int m, int e, int N) {
		return SquareAndMultiply.squareAndMultiply(m, e, N);
	}

	public int encrypt(int m) {
		return encrypt(m, e, N);
	}

	public int decrypt(int c, int d, int N) {
		// optional
		return SquareAndMultiply.squareAndMultiply(c, d, N);
	}

	public int decrypt(int c) {
		return decrypt(c, d, N);
	}
	
	public int sign(int message) {
		return SquareAndMultiply.squareAndMultiply(message, d, N);
	}
	
	public boolean checkSign(int message, int signature, int e, int N) {
		return (SquareAndMultiply.squareAndMultiply(signature, e, N)) == message;
	}

}
